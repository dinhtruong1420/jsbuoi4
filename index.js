function sapXep() {
	var soThuNhat = document.getElementById('sothunhat').value * 1;
	var soThuHai = document.getElementById('sothuhai').value * 1;
	var soThuBa = document.getElementById('sothuba').value * 1;
	if (soThuNhat > soThuHai && soThuNhat < soThuBa) {
		document.getElementById('result').innerHTML = `${soThuHai}<${soThuNhat}<${soThuBa}`;
	} else if (soThuBa > soThuHai && soThuHai > soThuNhat) {
		document.getElementById('result').innerHTML = `${soThuNhat}<${soThuHai}<${soThuBa}`;
	} else if (soThuHai > soThuNhat && soThuNhat > soThuBa) {
		document.getElementById('result').innerHTML = `${soThuBa}<${soThuNhat}<${soThuHai}`;
	} else if (soThuHai > soThuBa && soThuNhat < soThuBa) {
		document.getElementById('result').innerHTML = `${soThuNhat}<${soThuBa}<${soThuHai}`;
	} else if (soThuNhat > soThuBa && soThuBa > soThuHai) {
		document.getElementById('result').innerHTML = `${soThuHai}<${soThuBa}<${soThuNhat}`;
	} else
		document.getElementById('result').innerHTML = `${soThuBa}<${soThuHai}<${soThuNhat}`;
}

function guiLoiChao() {
	var select = document.getElementById('thanhvien');
	var thanhVien = select.options[select.selectedIndex].value;
	const bo = "Bố";
	const me = "Mẹ";
	const anhTrai = "Anh trai";
	const emGai = "Em gái";
	switch (thanhVien) {
		case bo: {
			return document.getElementById('result1').innerHTML = `Xin chào ${bo}!`;
			break;
		}
		case me: {
			return document.getElementById('result1').innerHTML = `Xin chào ${me}!`;
			break;
		}
		case anhTrai: {
			return document.getElementById('result1').innerHTML = `Xin chào ${anhTrai}!`;
			break;
		}
		case emGai: {
			return document.getElementById('result1').innerHTML = `Xin chào ${emGai}!`;
		}
	}
	console.log(thanhVien);

}

function demSoChanLe() {
	var soThuNhat = document.getElementById('soThuNhat').value * 1;
	var soThuHai = document.getElementById('soThuHai').value * 1;
	var soThuBa = document.getElementById('soThuBa').value * 1;
	var soLuongSoChan = 0;
	if (soThuNhat % 2 == 0) {
		soLuongSoChan += 1;
	}
	if (soThuHai % 2 == 0) {
		soLuongSoChan += 1;
	}
	if (soThuBa % 2 == 0) {
		soLuongSoChan += 1;
	}
	var soLuongSoLe = 3 - soLuongSoChan;
	document.getElementById('result2').innerHTML = `Có ${soLuongSoChan} số chẵn,${soLuongSoLe} số lẻ`
}

function duDoanHinh() {
	var doDaiCanh1 = document.getElementById('canh1').value * 1;
	var doDaiCanh2 = document.getElementById('canh2').value * 1;
	var doDaiCanh3 = document.getElementById('canh3').value * 1;
	var doDaiBinhPhuongCanh1 = Math.pow(doDaiCanh1, 2);
	var doDaiBinhPhuongCanh2 = Math.pow(doDaiCanh2, 2);
	var doDaiBinhPhuongCanh3 = Math.pow(doDaiCanh3, 2);
	if ((doDaiCanh1 == doDaiCanh2) && (doDaiCanh1 != doDaiCanh3) || (doDaiCanh1 == doDaiCanh3) && (doDaiCanh1 != doDaiCanh2) || (doDaiCanh2 == doDaiCanh3) && (doDaiCanh2 != doDaiCanh1)) {
		document.getElementById('result3').innerHTML = "Tam giác cân"
	} else if (doDaiCanh1 == doDaiCanh2 && doDaiCanh2 == doDaiCanh3 && doDaiCanh1 == doDaiCanh3) {
		document.getElementById('result3').innerHTML = "Tam giác đều"
	} else if ((doDaiBinhPhuongCanh1 == doDaiBinhPhuongCanh2 + doDaiBinhPhuongCanh3) || (doDaiBinhPhuongCanh2 == doDaiBinhPhuongCanh1 + doDaiBinhPhuongCanh3) || (doDaiBinhPhuongCanh3 == doDaiBinhPhuongCanh1 + doDaiBinhPhuongCanh2)) {
		document.getElementById('result3').innerHTML = "Tam giác vuông"
	} else
		document.getElementById('result3').innerHTML = "Một loại tam giác nào đó"
}